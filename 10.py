# -*- coding: utf-8 -*-

import threading, time

e = threading.Event()

def fun1():
    print("Funkcija ce spavati 5 s, a zatim postaviti da se dogodio dogadjaj.")
    time.sleep(15)
    print("fun1: postavljam event")
    e.set()

def fun2():
    print("fun2: Cekat cu 3 s.")
    e.wait(10)
    print("fun2: Ja sam cekao 3 sekunde na event. Ukoliko ga nemam, ispisujem ovu liniju odmah.")

def fun3():
    print("fun3: Cekam da se event postavi.")
    e.wait()
    print("fun3: Tko ceka, doceka.")

t1 = threading.Thread(target=fun1)
t2 = threading.Thread(target=fun2)
t3 = threading.Thread(target=fun3)

t1.start()
t2.start()
t3.start()