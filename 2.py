# -*- coding: utf-8 -*-

'''
Definirajte funkciju countdown_to_zero(n) koja prima kao argument
prirodni broj n i radi odbrojavanje, odnosno u while petlji
smanjue broj za 1 sve dok ne dođe do nule.

Pokrenite tri niti t1, t2, t3 redom za brojeve
651929250, 421858921, 2188312.
Pripazite da i ovdje (arg1,) nije isto što i (arg1).

U glavnom procesu "odspavajte" 5 sekundi, a zatim provjerite
jesu li živi t1, t2 i t3, i one koji jesu
pridružite na glavni proces. 

'''

import threading, time

def countdown_to_zero(n):
    t = n
    print("Pokrecem odbrojavanje.")
    while n > 1:
        n -= 1
    print("Gotova nit sa argumentom", t)

t1 = threading.Thread(target=countdown_to_zero, args=(651929250,))
t2 = threading.Thread(target=countdown_to_zero, args=(421858921,))
t3 = threading.Thread(target=countdown_to_zero, args=(2188312,))

t1.start()
t2.start()
t3.start()

print("Idem spavat 5 s.")
time.sleep(5)
print("Spavao sa 5 s.")


print("Pocinje blok sa if-ovima")

if t1.is_alive():
    print("t1 je ziv")
    t1.join()
else:
    print("t1 je mrtav")

if t2.is_alive():
    print("t2 je ziv")
    t2.join()
else:
    print("t2 je mrtav")

if t3.is_alive():
    print("t3 je ziv")
    t3.join()
else:
    print("t3 je mrtav")
    
print("Glavni program je gotov")