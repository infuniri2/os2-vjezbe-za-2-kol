import threading

semafor1 = threading.Semaphore(5)
knjiga1 = "Dostojevski: Z&K"
semafor2 = threading.Semaphore(3)
knjiga2 = "Tolstoj: AK"
semafor3 = threading.Semaphore(4)
knjiga3 = "Balzac: OG"

ucenici = ["Ucenik_1", "Ucenik_2", "Ucenik_3", "Ucenik_4", "Ucenik_5", "Ucenik_6", "Ucenik_7"]

def lektira(ime):
    print("Ja sam", ime, "i dosao/la sam u knjiznicu.")
    semafor1.acquire()
    print(ime, "cita", knjiga1, "i sara po margini.")
    semafor1.release()
    semafor2.acquire()
    print(ime, "cita", knjiga2, "i sara po margini.")
    semafor2.release()
    semafor3.acquire()
    print(ime, "cita", knjiga3, "i sara po margini.")
    semafor3.release()
    print("Ja sam", ime, "i vrato/la sam sve knjige.")

t = []

for x in ucenici:
    t.append(threading.Thread(target = lektira, args = (x,)))

print("7 učenika dolazi u knjižnicu")

for x in t:
    x.start()
