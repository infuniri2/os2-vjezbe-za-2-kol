# -*- coding: utf-8 -*-
'''
Promijenite program iz prethodnog zadatka da računa
produkt brojeva u danom rasponu umjesto zbroja.

Pored toga, usporedite performanse kod varijante koja dohvaća
lock u svakoj iteraciji for petlje s performansama varijante
koja to dohvaća lock prije for petlje. Koja je brža?
Objasnite zašto. (Raspon smanjite po potrebi.)

Naposlijetku, isprobajte korištenje višestrukog
zaključavanja umjesto običnog.
Ima li razlike u performansama?
Objasnite zašto. 

'''

import threading, time

zbroj = 1
zbroj_lock = threading.Lock()

def zbroj_ab(a, b):
    global zbroj
    zbroj_lock.acquire()
    for i in range(a, b + 1):
        #zbroj_lock.acquire()
        zbroj *= i
    zbroj_lock.release()

t1 = threading.Thread(target=zbroj_ab, args=(1, 250))
t2 = threading.Thread(target=zbroj_ab, args=(251, 500))

print("Vrijednost var na pocetku:", zbroj)

pocetak = time.time()
t1.start()
t2.start()

print("Glavni program moze raditi sto zeli dok zbroj racuna.")

t1.join()
t2.join()
kraj = time.time()

#print("t1", t1.is_alive())
#print("t2", t2.is_alive())

print("Zbroj na kraju:", zbroj)
print("Razlika u ms:", kraj-pocetak)
'''
zbroj2 = 0
for i in range(0, 500000 + 1):
    zbroj2 += i

print("zbroj2:", str(zbroj2))
'''