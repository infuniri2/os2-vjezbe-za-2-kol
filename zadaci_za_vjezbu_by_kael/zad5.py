# -*- coding: utf-8 -*-
"""

Napišite program koji koristi MPI za izračun zbroja kubova
brojeva u rasponu od 1 do 300000 u 6 procesa korištenjem
komunikacije točka-do točke. Raspon raspodijelite po procesima
po želji. Proces ranga 0 je onaj kojem će preostalih pet procesa
javiti svoje rezultate i koji će rezultate sumirati te ispisati
na ekran. Ostali procesi neka ne ispisuju ništa.
Kod slanja iskoristite neblokirajuću komunikaciju.  

"""

# mpirun -np 6 python3 zad5.py

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size == 6

if rank == 0:
    
    l1 = [i for i in range (1,50001)]
    l2 = [i for i in range (50001,100001)]
    l3 = [i for i in range (100001,150001)]
    l4 = [i for i in range (150001,200001)]
    l5 = [i for i in range (200001,250001)]
    l6 = [i for i in range (250001,300001)]
    
    req1 = comm.isend(l2, dest=1, tag=11)
    req2 = comm.isend(l3, dest=2, tag=22)
    req3 = comm.isend(l4, dest=3, tag=33)
    req4 = comm.isend(l5, dest=4, tag=44)
    req5 = comm.isend(l6, dest=5, tag=55)
    
    sumal1 = 0
    for i in l1:
        sumal1 += i**3
        
    sumal2 = comm.recv(source=1, tag=111)
    sumal3 = comm.recv(source=2, tag=222)
    sumal4 = comm.recv(source=3, tag=333)
    sumal5 = comm.recv(source=4, tag=444)
    sumal6 = comm.recv(source=5, tag=555)
    
    MPI.Request.Waitall([req1, req2, req3, req4, req5])
    
    suma = sumal1 + sumal2 + sumal3 + sumal4 + sumal5 + sumal6
    
    print("Zbroj je", suma)
    
elif rank == 1:
    l = comm.recv(source=0, tag=11)
    sumal = 0
    for i in l:
        sumal += i**3
    req = comm.isend(sumal, dest=0, tag=111)
    req.wait()
elif rank == 2:
    l = comm.recv(source=0, tag=22)
    sumal = 0
    for i in l:
        sumal += i**3
    req = comm.isend(sumal, dest=0, tag=222)
    req.wait()
elif rank == 3:
    l = comm.recv(source=0, tag=33)
    sumal = 0
    for i in l:
        sumal += i**3
    req = comm.isend(sumal, dest=0, tag=333)
    req.wait()
elif rank == 4:
    l = comm.recv(source=0, tag=44)
    sumal = 0
    for i in l:
        sumal += i**3
    req = comm.isend(sumal, dest=0, tag=444)
    req.wait()
elif rank == 5:
    l = comm.recv(source=0, tag=55)
    sumal = 0
    for i in l:
        sumal += i**3
    req = comm.isend(sumal, dest=0, tag=555)
    req.wait()
