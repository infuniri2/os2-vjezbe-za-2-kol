# -*- coding: utf-8 -*-
"""

Napišite program koji vrši zbroj kvadrata brojeva u rasponu
od 1 do 500000 u 3 niti (raspodijelite po želji).
Iskoristite višenitnosti i varijablu u koju ćete spremiti
zbroj učinite dijeljenom; iskoristite zaključavanje kod
promjene varijable u svakoj od niti;
napravite dijeljenu barijeru između tri niti tako da niti
čekaju jedna na drugu i istovremeno završavaju izvođenje. 

"""

import threading as th

suma = 0
lock = th.Lock()

barijera = th.Barrier(3)

def kvad(a, b):
    global suma
    lock.acquire()
    for i in range(a, b+1):
        suma += i**2
    lock.release()
    barijera.wait()
    
t1 = th.Thread(target=kvad, args=(1, 166666))
t2 = th.Thread(target=kvad, args=(166667, 333333))
t3 = th.Thread(target=kvad, args=(333334, 500000))

t1.start()
t2.start()
t3.start()

t1.join()
t2.join()
t3.join()

print("Suma je", suma)