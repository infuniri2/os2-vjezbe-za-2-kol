# -*- coding: utf-8 -*-
"""

Napišite program koji koristi MPI za izračun zbroja kvadrata brojeva
u rasponu od 1 do 500000 u 3 procesa korištenjem komunikacije
točka-do točke, i to tako da proces ranga 0 šalje procesu
ranga 1 i procesu ranga 2 liste koje sadrže brojeve
od 1 do 250000 i od 250000 do 500000 (respektivno).
Procesi ranga 1 i 2 računaju zbroj kvadrata brojeva koje su dobili.
Procesu ranga 0 procesi ranga 1 i 2 javljaju rezultate koje su dobili.
Proces ranga 0 prima oba rezultata i njihov zbroj ispisuje na ekran.
Iskoristite blokirajuću komunikaciju.  

"""

# mpirun -np 3 python3 zad2.py

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size == 3

if rank == 0:
    l1 = [i for i in range(1, 250001)]
    l2 = [i for i in range(250001, 500001)]
    
    comm.send(l1, dest=1, tag=11)
    comm.send(l2, dest=2, tag=22)
    
    r1 = comm.recv(source=1, tag=33)
    r2 = comm.recv(source=2, tag=44)
    
    print("Zbroj je", r1+r2)
    
elif rank == 1:
    
    recvl1 = comm.recv(source=0, tag=11)
    z1 = 0
    
    for i in recvl1:
        z1 += i**2
        
    comm.send(z1, dest=0, tag=33)
    
elif rank == 2:
    
    recvl2 = comm.recv(source=0, tag=22)
    z2 = 0
    
    for i in recvl2:
        z2 += i**2
        
    comm.send(z2, dest=0, tag=44)
        
else:
    exit()






