# -*- coding: utf-8 -*-
"""

Napišite program koji koristi MPI za izračun zbroja kvadrata brojeva
u rasponu od 1 do 500000 u 4 procesa korištenjem kolektivne
komunikacije tipa scatter-reduce. Raspodijelite po želji;
za to morate napraviti listu oblika 

brojevi = [[1, 2, 3, ..., 125000],
           [125001, 125002, ..., 250000],
           [250001, 250002, ..., 375000],
           [375001, 375002, ..., 500000]]

odnosno potrebno je da ima 4 podliste od kojih će svaka biti
dana odgovarajućem procesu. Svi procesi računaju zbroj
kvadrata brojeva koje su dobili. Nakon završetka obrade na
procesu ranga 0 sakupite rezultate i to tako da izvršite 
redukciju korištenjem sumiranja. 

"""

# mpirun -np 4 python3 zad3.py

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size == 4

if rank == 0:
    
    red1 = [i for i in range(1, 125001)]
    red2 = [i for i in range(125001, 250001)]
    red3 = [i for i in range(250001, 375001)]
    red4 = [i for i in range(375001, 500001)]

    brojevi = []
    brojevi.append(red1)
    brojevi.append(red2)
    brojevi.append(red3)
    brojevi.append(red4)
else:
    brojevi = None


red = comm.scatter(brojevi, root=0)
suma_retka = 0

for i in red:
    suma_retka += i**2

suma_matrice = comm.reduce(suma_retka, op=MPI.SUM, root=0)

if rank == 0:
    print ("Zbroj je:", suma_matrice)
