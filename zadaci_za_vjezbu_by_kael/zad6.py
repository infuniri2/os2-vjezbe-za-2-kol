# -*- coding: utf-8 -*-
"""

Napišite program koji koristi MPI za izračun zbroja kubova brojeva
u rasponu od 1 do 300000 u 6 procesa korištenjem kolektivne
komunikacije tipa scatter-reduce.
Na procesu ranga 0 inicijalizirajte listu pojedinih
raspona i raspodijelite je procesima koji kubiraju dobivene
brojeve i zbrajaju ih. Nakon završetka obrade na procesu ranga 0
izvedite redukciju sumiranjem i na procesu ranga 0 ispišite
rezultat na ekran. Ostali procesi neka ne ispisuju ništa. 

"""

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size == 6


if rank == 0:
    
    l1 = [i for i in range (1,50001)]
    l2 = [i for i in range (50001,100001)]
    l3 = [i for i in range (100001,150001)]
    l4 = [i for i in range (150001,200001)]
    l5 = [i for i in range (200001,250001)]
    l6 = [i for i in range (250001,300001)]
    
    l = []
    
    l.append(l1)
    l.append(l2)
    l.append(l3)
    l.append(l4)
    l.append(l5)
    l.append(l6)
else:
    l = None
    
red = comm.scatter(l, root=0)

suma_red = 0
    
for i in red:
    suma_red += i**3
    
suma_l = comm.reduce(suma_red, op=MPI.SUM, root=0)

if rank==0:
    print("Zbroj je", suma_l)
