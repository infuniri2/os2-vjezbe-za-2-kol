# -*- coding: utf-8 -*-
"""

Napišite program koji vrši zbroj kubova brojeva u rasponu
od 1 do 300000 u 3 niti i raspodijelite tako da
1. nit računa raspon od 1 do 100000,
2. nit od 100001 do 200000,
3. nit od 200001 do 300000.
Iskoristite višenitnosti i varijablu u koju ćete spremiti
zbroj učinite dijeljenom;
iskoristite zaključavanje kod promjene varijable u svakoj od niti;
napravite događaj koji postavlja 3. nit u trenutku kad
završi s izvođenjem, i učinite da na njega čekaju preostale
dvije niti. 

"""

import threading as th

zbroj = 0
lock = th.Lock()
e = th.Event()

def kub(a, b):
    global zbroj
    if a!=200001:
        e.wait()
    lock.acquire()
    for i in range(a, b+1):
        zbroj += i**3
    lock.release()
    if a==200001:
        e.set()

t1 = th.Thread(target = kub, args = (1, 100000))
t2 = th.Thread(target = kub, args = (100001, 200000))
t3 = th.Thread(target = kub, args = (200001, 300000))


t1.start()
t2.start()
t3.start()

t1.join()
t2.join()
t3.join()

print("Zbroj je", zbroj)