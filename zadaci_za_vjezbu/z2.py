"""
Napišite program koji koristi MPI za izračun zbroja kvadrata brojeva u rasponu od 1 do 500000 u 3 procesa korištenjem komunikacije točka-do točke, i to tako da proces ranga 0 šalje procesu ranga 1 i procesu ranga 2 liste koje sadrže brojeve od 1 do 250000 i od 250000 do 500000 (respektivno). Procesi ranga 1 i 2 računaju zbroj kvadrata brojeva koje su dobili. Procesu ranga 0 procesi ranga 1 i 2 javljaju rezultate koje su dobili. Proces ranga 0 prima oba rezultata i njihov zbroj ispisuje na ekran. Iskoristite blokirajuću komunikaciju. 
"""
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 3:
    exit()

if rank == 0:
    lista1 = list(range(1, 250000+1))
    lista2 = list(range(250000, 500000+1))
    comm.send(lista1, dest = 1, tag = 11)
    comm.send(lista2, dest = 2, tag = 22)
    suma1 = comm.recv(source = 1, tag = 111)
    suma2 = comm.recv(source = 2, tag = 222)
    print("Suma:", suma1 + suma2)
if rank == 1:
    lista1 = comm.recv(source = 0, tag = 11)
    suma = 0
    for i in lista1:
        suma += i**2
    comm.send(suma, dest = 0, tag = 111)
if rank == 2:
    lista2 = comm.recv(source = 0, tag = 22)
    suma = 0
    for i in lista2:
        suma += i**2
    comm.send(suma, dest = 0, tag = 222)
else:
    None
