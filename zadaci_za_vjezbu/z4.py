"""
Napišite program koji vrši zbroj kubova brojeva u rasponu od 1 do 300000 u 3 niti i raspodijelite tako da 1. nit računa raspon od 1 do 100000, 2. nit od 100001 do 200000, 3. nit od 200001 do 300000. Iskoristite višenitnosti i varijablu u koju ćete spremiti zbroj učinite dijeljenom; iskoristite zaključavanje kod promjene varijable u svakoj od niti; napravite događaj koji postavlja 3. nit u trenutku kad završi s izvođenjem, i učinite da na njega čekaju preostale dvije niti. 
"""
import threading

suma = 0
suma_lock = threading.Lock()
event = threading.Event()

def sumiraj(n, m, x):
    if x == 0:
        event.wait()
    suma_lock.acquire()
    global suma
    for i in range(n, m+1):
        suma += i**3
    suma_lock.release()
    if x == 1:
        event.set()

t1 = threading.Thread(target = sumiraj, args = (1, 100000, 0))
t2 = threading.Thread(target = sumiraj, args = (100001, 200000, 0))
t3 = threading.Thread(target = sumiraj, args = (200001, 300000, 1))

t1.start()
t2.start()
t3.start()

t1.join()
t2.join()
t3.join()

print("Suma kubova:", suma)
