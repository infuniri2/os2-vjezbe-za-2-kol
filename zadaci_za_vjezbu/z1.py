"""
Napišite program koji vrši zbroj kvadrata brojeva u rasponu od 1 do 500000 u 3 niti (raspodijelite po želji). Iskoristite višenitnosti i varijablu u koju ćete spremiti zbroj učinite dijeljenom; iskoristite zaključavanje kod promjene varijable u svakoj od niti; napravite dijeljenu barijeru između tri niti tako da niti čekaju jedna na drugu i istovremeno završavaju izvođenje. 
"""
import threading

suma = 0
bar = threading.Barrier(3)
suma_lock = threading.Lock()

def sumiraj(n, m):
    suma_lock.acquire()
    global suma
    for i in range(n , m+1):
        suma += i
    suma_lock.release()
    bar.wait()

t1 = threading.Thread(target = sumiraj, args = (1, 200000))
t2 = threading.Thread(target = sumiraj, args = (200001, 400000))
t3 = threading.Thread(target = sumiraj, args = (400001, 500000))

t1.start()
t2.start()
t3.start()

t1.join()
t2.join()
t3.join()

print("Suma:", suma)
