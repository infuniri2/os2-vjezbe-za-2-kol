"""
Napišite program koji koristi MPI za izračun zbroja kubova brojeva u rasponu od 1 do 300000 u 6 procesa korištenjem komunikacije točka-do točke. Raspon raspodijelite po procesima po želji. Proces ranga 0 je onaj kojem će preostalih pet procesa javiti svoje rezultate i koji će rezultate sumirati te ispisati na ekran. Ostali procesi neka ne ispisuju ništa. Kod slanja iskoristite neblokirajuću komunikaciju. 
"""
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# 3 procesa

if size < 3:
    exit()

if rank == 0:
    suma = 0
    for i in range(1, 2+1):
        suma += comm.recv(source = i, tag = 77)
    print(suma)
elif rank == 1:
    suma = 0
    for i in range(1, 150000+1):
        suma += i**3
    target = 0
elif rank == 2:
    suma = 0
    for i in range(150001, 300000+1):
        suma += i**3
    target = 0

if rank != 0:
    req = comm.isend(suma, dest = target, tag = 77)
    req.Wait()
