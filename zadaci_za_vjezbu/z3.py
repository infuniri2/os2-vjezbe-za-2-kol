"""
Napišite program koji koristi MPI za izračun zbroja kvadrata brojeva u rasponu od 1 do 500000 u 4 procesa korištenjem kolektivne komunikacije tipa scatter-reduce. Raspodijelite po želji; za to morate napraviti listu oblika

brojevi = [[1, 2, 3, ..., 125000],
           [125001, 125002, ..., 250000],
           [250001, 250002, ..., 375000],
           [375001, 375002, ..., 500000]]

odnosno potrebno je da ima 4 podliste od kojih će savka biti dana odgovarajućem procesu. Svi procesi računaju zbroj kvadrata brojeva koje su dobili. Nakon završetka obrade na procesu ranga 0 sakupite rezultate i to tako da izvršite redukciju korištenjem sumiranja. 
"""
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if size < 4:
    exit()

if rank == 0:
    brojevi = [list(range(1, 125000+1)),
               list(range(125001, 250000+1)),
               list(range(250001, 375000+1)),
               list(range(375001, 500000+1))]
else:
    brojevi = None

lista = comm.scatter(brojevi, root = 0)

zbroj = 0
for i in lista:
    zbroj += i**2

total_z = comm.reduce(zbroj, op = MPI.SUM, root = 0)

if rank == 0:
    print("Total:", total_z)
