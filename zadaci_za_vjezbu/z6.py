"""
Napišite program koji koristi MPI za izračun zbroja kubova brojeva u rasponu od 1 do 300000 u 6 procesa korištenjem kolektivne komunikacije tipa scatter-reduce. Na procesu ranga 0 inicijalizirajte listu pojedinih raspona i raspodijelite je procesima koji kubiraju dobivene brojeve i zbrajaju ih. Nakon završetka obrade na procesu ranga 0 izvedite redukciju sumiranjem i na procesu ranga 0 ispišite rezultat na ekran. Ostali procesi neka ne ispisuju ništa. 
"""
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# 3 procesa

if size < 3:
    exit()

if rank == 0:
    brojevi = [list(range(1, 100000+1)),
               list(range(100001, 200000+1)),
               list(range(200001, 300000+1))]
else:
    brojevi = None

lista = comm.scatter(brojevi, root = 0)

kubovi = 0

for i in lista:
    kubovi += i**3

total = comm.reduce(kubovi, op = MPI.SUM, root = 0)

if rank == 0:
    print("Suma kubova:", total)
