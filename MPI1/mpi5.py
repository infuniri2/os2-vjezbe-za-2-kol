from mpi4py import MPI
comm = MPI.COMM_WORLD

rank = comm.Get_rank()
size = comm.Get_size()

sendmsg = [rank] * 3
right = (rank + 1) % size
left = (rank - 1) % size

req1 = comm.isend(sendmsg, dest=right)
req2 = comm.isend(sendmsg, dest=left)
lmsg = comm.recv(source=left)
rmsg = comm.recv(source=right)

MPI.Request.Waitall([req1, req2])
assert lmsg == [left] * 3
assert rmsg == [right] * 3
