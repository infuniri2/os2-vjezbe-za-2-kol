from mpi4py import MPI
import time
comm = MPI.COMM_WORLD
if comm.Get_size()!= 2:
    print("Moraju postojati barem 2 procesa")    
    exit()

if comm.Get_rank() == 0:
    print("Proces 0 ide spavat 3 sec")
    time.sleep(3)
    print("Proces 0 se probudio")
    sendmsg = 777
    target = 1
else:
    print("Proces 1 ide spavat 5 sec")
    time.sleep(5)
    print("Proces 1 se probudio")
    sendmsg = "abc"
    target = 0

print("Proces",comm.Get_rank(),"pocinje slati",sendmsg)
request = comm.isend(sendmsg, dest=target, tag=77)
#comm.send(sendmsg, dest=target, tag=77)
print("Proces",comm.Get_rank(),"poslao",sendmsg," i primat ce od procesa", target)
recvmsg = comm.recv(source=target, tag=77)
print("Proces",comm.Get_rank(),"primio",recvmsg,"od procesa", target, "i prelazi u wait")
request.Wait()
print("Proces",comm.Get_rank(),"zavrsio Wait(), kraj programa.")
