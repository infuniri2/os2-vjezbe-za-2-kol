from mpi4py import MPI

comm = MPI.COMM_WORLD
#assert comm.Get_size() == 2

rank = comm.Get_rank()
size = comm.Get_size()

if size < 2:
    exit()

if rank == 0:
    sendmsg = 777
    comm.send(sendmsg, dest=1, tag=55)
    recvmsg = comm.recv(source=1, tag=77)
elif rank == 1:
    recvmsg = comm.recv(source=0, tag=55)
    sendmsg = "abc"
    comm.send(sendmsg, dest=0, tag=77)

print ("Proces", rank, "je poslao", sendmsg)
print ("Proces", rank, "je primio", recvmsg)
