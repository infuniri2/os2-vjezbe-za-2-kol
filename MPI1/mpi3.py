from mpi4py import MPI
import math

comm = MPI.COMM_WORLD
#assert comm.Get_size() == 3

rank = comm.Get_rank()
size = comm.Get_size()

if size != 3:
    exit()

if rank == 0:
    recvmsg1 = comm.recv(source=1, tag=11)
    recvmsg2 = comm.recv(source=2, tag=22)
    print ("Proces", rank, "je primio", recvmsg1)
    print ("Proces", rank, "je primio", recvmsg2)
elif rank == 1:
    sendmsg1 = 0
    for i in range(1,101):
        sendmsg1 += i
    comm.send(sendmsg1, dest=0, tag=11)
elif rank == 2:
    sendmsg2 = math.factorial(100)
    comm.send(sendmsg2, dest=0, tag=22)
