from mpi4py import MPI
import time

rank = MPI.COMM_WORLD.Get_rank()
size = MPI.COMM_WORLD.Get_size()
name = MPI.Get_processor_name()

time.sleep(rank)

# print("Pozdrav od procesa ranga %d od ukupno %d procesa na domacinu %s" % (rank, size, name))

if (rank % 2 == 0):
    print ("Ja sam parni proces broj", rank)
else:
    print ("Ja sam neparni proces broj", rank, ", a kvadrat mog identifikatora je", rank**2)
