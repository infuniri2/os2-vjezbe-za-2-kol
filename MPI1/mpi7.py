'''
Dodajte proces ranga 3 i ucinite da mu oba procesa (ranga 1 i 2)
koristenjem blokirajuce komunikacije posalju svoje rezultate.
On prima oba broja i racuna njihov produkt, te ga salje
procesu ranga 0 koji ga ispisuje na ekran.

'''

from mpi4py import MPI
import math

comm = MPI.COMM_WORLD
#assert comm.Get_size() == 3

rank = comm.Get_rank()
size = comm.Get_size()

if size != 4:
    exit()

if rank == 0:
    recvmsg3 = comm.recv(source=3, tag=33)
    print ("Proces", rank, "je primio", recvmsg3)
elif rank == 1:
    sendmsg1 = 0
    for i in range(1,101):
        sendmsg1 += i
    comm.send(sendmsg1, dest=3, tag=11)
elif rank == 2:
    sendmsg2 = math.factorial(100)
    comm.send(sendmsg2, dest=3, tag=22 )
elif rank == 3:
    recvmsg1 = comm.recv(source=1, tag=11)
    recvmsg2 = comm.recv(source=2, tag=22)
    comm.send((recvmsg1 * recvmsg2), dest=0, tag=33)

#req1 = comm.isend(sendmsg1, dest=0, tag=11)
#req2 = comm.isend(sendmsg2, dest=0, tag=22)

#recvmsg1 = comm.recv(source=1, tag=11)
#recvmsg2 = comm.recv(source=2, tag=22)

#MPI.Request.Waitall([req1, req2])

