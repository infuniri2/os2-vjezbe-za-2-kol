# -*- coding: utf-8 -*-
"""

Pečete Ledo Croissante koji zahtijevaju 15 minuta za odmrzavanje i
20 minuta u pećnici. Napravite dvije procesne niti,
od kojih jedna pokreće funkciju odmrzavanje(n),
gdje n broj minuta koje se kroasani odmrzavaju,
i drugu koja pokreće funkciju pecnica(n),
gdje je n broj minuta koje se kroasani peku u pećnici.
Pokrenite istovremeno obje niti, ali učinite da druga nit čeka
na događaj odmrzavanja kroasana koji prva nit postavlja. 

"""

import threading as t, time

e = t.Event()

def odmrzavanje(n):
    print("Krece odmrzavanje")
    time.sleep(n)
    print("Kroasani odmrznuti")
    e.set()

def pecnica(n):
    print("Pecnica ceka na odmrzavanje")
    e.wait()
    print("Pecnica krece peci")
    time.sleep(n)
    print("Kroasani gotovi, dobar tek!")


t1 = t.Thread(target=odmrzavanje, args=(15*60, ))
t2 = t.Thread(target=pecnica, args=(20*60, ))

t1.start()
t2.start()