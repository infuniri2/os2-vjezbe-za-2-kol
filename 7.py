# -*- coding: utf-8 -*-
"""

Knjižnica nudi tri knjige:
 * Marko Marulić: Judita, 5 komada
 * Fjodor Mihajlovič Dostojevski: Zločin i kazna, 3 komada
 * Albert Camus: Stranac, 4 komada

Reprezentirajte te tri knjige u programu kao tri niza znakova
proizvoljnog sadržaja, i pridružite im semafore s odgovarajućom vrijednosti.
Hoćete li koristiti ograničene semafore? Objasnite zašto.

Napišite funkciju `lektira()` koja prima jedan argument tipa znakovni niz,
a to je ime učenika, koje se zatim ispisuje na ekran
(npr. `"Ja sam Domagoj i došao sam u knjižnicu"`).
Učenik zatim "posuđuje te tri knjige", odnosno dohvaća njihove semafore
i radi na njima `acquire()`, pa "čita te knjige i šara na margini",
odnosno prvo ispisuje znakovne nizove na ekran, a zatim dopisuje na kraj
proizvoljan sadržaj. Naposlijetku "vraća te tri knjige", odnosno radi
`release()`.

Učinite da "7 učenika dolazi u knjižnicu", odnosno pokrenite 7 procesnih
niti za učenike imena redom
Domagoj, Ivan, Luka, Snežana, Romana, Sonja, Marta.


"""


import threading

k1 = ["Početak knjige k1:"]
s1 = threading.BoundedSemaphore(5)

k2 = ["Početak knjige k2:"]
s2 = threading.BoundedSemaphore(3)

k3 = ["Početak knjige k3:"]
s3 = threading.BoundedSemaphore(4)

def lektira(ime):
    print("Ja sam",ime,"i došao/la sam u knjižnicu")
    global k1, k2, k3
    
    s1.acquire()
    print(k1)
    ubaciti = "Novi sadrzaj u k1 by",ime, "\n"
    k1.append(ubaciti)
    s1.release()
    
    
    s2.acquire()
    print(k2)
    ubaciti = "Novi sadrzaj u k2 by",ime, "\n"
    k2.append(ubaciti)
    s2.release()
    
    
    s3.acquire()
    print(k3)
    ubaciti = "Novi sadrzaj u k3 by",ime, "\n"
    k3.append(ubaciti)
    s3.release()
    
    print(ime, "vraća te tri knjige")

u1 = threading.Thread(target=lektira, args=("Domagoj",))
u2 = threading.Thread(target=lektira, args=("Ivan",))
u3 = threading.Thread(target=lektira, args=("Luka",))
u4 = threading.Thread(target=lektira, args=("Snežana",))
u5 = threading.Thread(target=lektira, args=("Romana",))
u6 = threading.Thread(target=lektira, args=("Sonja",))
u7 = threading.Thread(target=lektira, args=("Marta",))


print("7 učenika dolazi u knjižnicu")

u1.start()
u2.start()
u3.start()
u4.start()
u5.start()
u6.start()
u7.start()

u1.join()
u2.join()
u3.join()
u4.join()
u5.join()
u6.join()
u7.join()

print("7 učenika je međusobno svršilo")
