# mpirun -np 6 python3 mpi2-6.py

'''

Zadatak:

Skalarni produkt za 2 vektora a i b u vektorskom prostoru
dimenzije je:
a[0] * b[0] + a[1] * b[1] + ... + a[n] * b[n]



'''

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = [1, 2, 3, 4, 5, 6]
    b = [7, 8, 9, 10, 11, 12]
else:
    a = None
    b = None

komp_od_a = comm.scatter(a, root=0)
komp_od_b = comm.scatter(b, root=0)

produkt_komp = komp_od_a * komp_od_b

# produkt_vektora = comm.gather(produkt_komp, root = 0)

'''
if rank == 0:
    # print(produkt_vektora)
    skalarni_produkt = 0
    for x in produkt_vektora:
        skalarni_produkt += x
    print("Skalarni produkt iznosi", skalarni_produkt)
'''

skalarni_produkt_vektora = comm.reduce(produkt_komp, op = MPI.SUM, root = 0)
if rank == 0:
    print("Skalarni produkt iznosi", skalarni_produkt_vektora)
