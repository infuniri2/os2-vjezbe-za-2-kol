# mpirun -np 6 python3 mpi2-7.py

'''

Zadatak:

Za 2 vektora izracunajte sumu pojedinih komponenata na razlicitim procesima, a zatim izvedite:
 * redukciju operacijom MPI.MAX (dakle, izracunate maksimum medju dobivenim rezultatima),
 * redukciju operacijom MPI.MIN (dakle, izracunate minimum medju dobivenim rezultatima)

'''

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = [1, 2, 3, 4, 5, 6]
    b = [7, 8, 9, 10, 11, 12]
    # min = 7 + 1 = 8
    # max = 6 + 12 = 18
else:
    a = None
    b = None

komp_od_a = comm.scatter(a, root=0)
komp_od_b = comm.scatter(b, root=0)

suma_komp = komp_od_a + komp_od_b

max_komp = comm.reduce(suma_komp, op = MPI.MAX, root = 0)
min_komp = comm.reduce(suma_komp, op = MPI.MIN, root = 0)

if rank == 0:
    print("Maksimum", max_komp)
    print("Minimum", min_komp)
    
