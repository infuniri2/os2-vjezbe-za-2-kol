# mpirun -np 4 python3 mpi2-2.py

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = [1, 2, 3, 4, 5]
    b = [5, 6, 7, 8, 2]
else:
    a = None
    b = None

komp_od_a = comm.scatter(a, root=0)
komp_od_b = comm.scatter(b, root=0)
zbroj = komp_od_a + komp_od_b

print("Proces ranga", rank, "zbraja", komp_od_a, "i", komp_od_b, "i dobije rez =", zbroj)
