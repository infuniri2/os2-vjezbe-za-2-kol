# mpirun -np 4 python3 mpi2-1.py

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

assert size == 4

if rank == 0:
    a = [1, 2, 3, 4]
    b = [5, 6, 7, 8]
    zbroj = [0, 0, 0, 0]

    '''
    for i in range(4):
        zbroj[i] = a[i] + b[i]
    print(zbroj)
    '''

    zbroj[0] = a[0] + b[0]

    comm.send(a[1], dest = 1, tag = 11)
    comm.send(b[1], dest = 1, tag = 22)
    
    comm.send(a[2], dest = 2, tag = 33)
    comm.send(b[2], dest = 2, tag = 44)
    
    comm.send(a[3], dest = 3, tag = 55)
    comm.send(b[3], dest = 3, tag = 66)

    zbroj[1] = comm.recv(source = 1, tag = 1)
    zbroj[2] = comm.recv(source = 2, tag = 2)
    zbroj[3] = comm.recv(source = 3, tag = 3)

    print(zbroj)

elif rank == 1:
    recv_a = comm.recv(source = 0, tag = 11)
    recv_b = comm.recv(source = 0, tag = 22)

    comm.send(recv_a + recv_b, dest = 0, tag = 1) #itd. za svaki od procesa ranga 1, 2, 3
elif rank == 2:
    recv_a = comm.recv(source = 0, tag = 33)
    recv_b = comm.recv(source = 0, tag = 44)
    
    comm.send(recv_a + recv_b, dest = 0, tag = 2) #itd. za svaki od procesa ranga 1, 2, 3    
elif rank == 3:
    recv_a = comm.recv(source = 0, tag = 55)
    recv_b = comm.recv(source = 0, tag = 66)

    comm.send(recv_a + recv_b, dest = 0, tag = 3) #itd. za svaki od procesa ranga 1, 2, 3
else:
    exit()
