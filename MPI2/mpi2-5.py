# mpirun -np 6 python3 mpi2-5.py

'''

Zadatak:

Ucinite da se na isti nacin racuna produkt vektora po komponentama
(a[i] * b[i] * c[i])
Racunanje produkta dodajte u trenutni program, nemojte njime zamijeniti racunanje zbroja.

Napomena:
Uocite da cete trebati jos jedan gather()

'''

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = [1, 2, 3, 4, 5, 6]
    b = [7, 8, 9, 10, 11, 12]
    c = [13, 14, 15, 16, 17, 18]
else:
    a = None
    b = None
    c = None

komp_od_a = comm.scatter(a, root=0)
komp_od_b = comm.scatter(b, root=0)
komp_od_c = comm.scatter(c, root=0)
zbroj = komp_od_a + komp_od_b + komp_od_c
produkt = komp_od_a * komp_od_b * komp_od_c

print("Proces ranga", rank, "zbraja", komp_od_a, ",", komp_od_b, "i", komp_od_c, "i dobije rez =", zbroj)
print("Proces ranga", rank, "mnozi", komp_od_a, ",", komp_od_b, "i", komp_od_c, "i dobije rez =", produkt)

zbroj_vektora = comm.gather(zbroj, root=0)
produkt_vektora = comm.gather(produkt, root=0)

if rank == 0:
    print("Zbroj vektora iznosi", zbroj_vektora)
    print("Produkt vektora iznosi", produkt_vektora)
