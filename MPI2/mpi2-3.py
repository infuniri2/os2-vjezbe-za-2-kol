# mpirun -np 6 python3 mpi2-3.py

'''

Zadatak:

Ucinite da a i b imaju 6 komponenata i pokrenite program u 6 procesa.
Dodajte treci vektor c i ucinite da se racuna zbroj
tri vektora umjesto zbroja 2 vektora.

'''

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = [1, 2, 3, 4, 5, 6]
    b = [7, 8, 9, 10, 11, 12]
    c = [13, 14, 15, 16, 17, 18]
else:
    a = None
    b = None
    c = None

komp_od_a = comm.scatter(a, root=0)
komp_od_b = comm.scatter(b, root=0)
komp_od_c = comm.scatter(c, root=0)
zbroj = komp_od_a + komp_od_b + komp_od_c

print("Proces ranga", rank, "zbraja", komp_od_a, ",", komp_od_b, "i", komp_od_c, "i dobije rez =", zbroj)
