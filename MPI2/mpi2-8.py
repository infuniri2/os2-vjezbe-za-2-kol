# mpirun -np 3 python3 mpi2-8.py

'''

Zadatak:

Matrice u Pythonu su oblika:

a = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

b = [[10, 20, 30],
     [40, 50, 60],
     [70, 80, 90]]
     

Napravite kod koji vrsi distribuirano zbrajanje dvaju matrica.
Jednostavnija varijanta je ta da izvedete zbroj matrica formata 3x3 u 3 procesa, tako da
svaki proces zbraja po 3 elementa,

npr.
proces ranga 0 ce zbrajati vektore [1, 2, 3] i [10, 20, 30] i to ce raditi serijski,
proces ranga 1 ce zbrajati 2. redak od a s 2.retjin id b serijski, itd.


Slozenija varijanta je da izvedete to u 9 procesa, ali njome se necemo ovdje baviti

'''

###########################
# JEDNOSTAVNIJA VARIJANTA #
###########################

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if rank == 0:
    a = [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]]

    b = [[10, 20, 30],
         [40, 50, 60],
         [70, 80, 90]]
else:
    a = None
    b = None

komp_od_a = comm.scatter(a, root=0)
komp_od_b = comm.scatter(b, root=0)
suma_komp = [0, 0, 0]

for i in range(0,3):
    suma_komp[i] = komp_od_a[i] + komp_od_b[i]
    
'''
suma_komp[0] = [11, 22, 33]
suma_komp[1] = [44, 55, 66]
suma_komp[2] = [77, 88, 99]
'''

mat_sum = comm.gather(suma_komp, root=0)

if rank == 0:
    print(mat_sum)