# -*- coding: utf-8 -*-
'''
Napišite program koji računa zbroj prvih 500'000
prirodnih brojeva u dvije procesne niti;
definirajte globalnu varijablu zbroj,
i učinite da jedna procesna nit u globalnu varijablu zbraja
brojeve od 1 do 250'000, a druga od 250'001 do 500'000.

Izvedite program bez zaključavanja, s običnim zaključavanjem
i s višestrukim zaključavanjem.
Ima li ovdje potrebe za višestrukim zaključavanjem?
Objasnite zašto.
Nema jer je KaeL tako rekao
'''

import threading

zbroj = 0
zbroj_lock = threading.RLock()

def zbroj_ab(a, b):
    global zbroj
    if a!=b:
        zbroj_lock.acquire()
        
        #for i in range(a, b + 1):
        #    zbroj += i
        zbroj += b 
        zbroj_ab(a,b-1)  
        
        zbroj_lock.release()
        
        
t1 = threading.Thread(target=zbroj_ab, args=(1, 250))
t2 = threading.Thread(target=zbroj_ab, args=(251, 500))

print("Vrijednost var na pocetku:", zbroj)

t1.start()
t2.start()

print("Glavni program moze raditi sto zeli dok zbroj racuna.")

t1.join()
t2.join()

#print("t1", t1.is_alive())
#print("t2", t2.is_alive())

print("Zbroj na kraju:", zbroj)

'''
zbroj2 = 0
for i in range(0, 500000 + 1):
    zbroj2 += i

print("zbroj2:", str(zbroj2))
'''