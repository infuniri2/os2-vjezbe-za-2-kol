# -*- coding: utf-8 -*-

import threading
import time

semafor = threading.Semaphore(1)

def funkcija(x):
    semafor.acquire()
    print("Ja sam nit", x, "i radim sa semaforom")
    time.sleep(2)
    semafor.release()
    print("Ja sam nit", x, "i zavrsila sam sa semaforom.")

t1 = threading.Thread(target=funkcija, args=(1,))
t2 = threading.Thread(target=funkcija, args=(2,))
t3 = threading.Thread(target=funkcija, args=(3,))

t1.start()
t2.start()
t3.start()
